<?php
/**
 * Created by PhpStorm.
 * User: pc
 * Date: 21.04.2018
 * Time: 16:35
 */

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Acl\Exception\Exception;
use Symfony\Component\HttpFoundation\Response;

class FileController extends Controller {

    /**
     * @Route("/delete-file", name="delete_file")
     * @Method({"POST"})
     * @param Request $request
     * @throws
     * @return Response
     */
     public function deleteAction(Request $request) {
        $id = (int)$request->request->get('id');
        $em = $this->getDoctrine()->getEntityManager();
        $file = $em->getRepository('AppBundle:File')->find($id);

        if (!$file) {
            throw $this->createNotFoundException('No file found for id '.$id);
        }

        /**@var \AppBundle\Service\FileService $fileLoader */
        $fileLoader = $this->container->get('app.files');
        $response = new Response();

        try{
            $fileLoader->delete($file);
            $response->setContent(json_encode(['code' => 200, 'message' => 'Ok']));
        } catch(Exception $e) {
            $response->setContent(json_encode(['code' => $e->getCode(), 'message' => $e->getMessage()]));
        }

        $em->remove($file);
        $em->flush();
         $response->headers->set('Content-Type', 'application/json');

         return $response;
    }
}