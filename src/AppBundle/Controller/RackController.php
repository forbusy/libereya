<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Rack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Translation\Translator;

/**
 * Rack controller.
 *
 * @Route("rack")
 */
class RackController extends Controller {

    /**
     * Name of entity for template
     *
     * @var string $entity
     */
    public $entity = 'rack';

    /**
     * List all racks entities
     *
     * @Route("/", name="rack_index")
     * @Method("GET")
     * @param array  $error
     * @return Response
     */
    public function indexAction($error = []) {
        $user = $this->getUser();
        $racks = $user->getRacks()->getValues();
        $entity = 'rack';
        $title = 'List of racks';
        $actions = ['delete', 'books', 'edit'];

        return $this->render('default/list.html.twig',
            ['entityName' => $this->entity,
             'entities' => $racks,
             'model' => $entity,
             'title' => $title,
             'fields' => Rack::$fields,
             'error'  => $error,
             'actions' => $actions]);
    }

    /**
     * Creates a new rack entity.
     *
     * @Route("/new", name="rack_new")
     * @Method({"GET", "POST"})
     * @param Request $request
     *
     * @return Response
     */
    public function newAction(Request $request) {
        $rack = new Rack();
        $user = $this->getUser();
        $rack->setUser($user);
        $form = $this->createForm('AppBundle\Form\RackType', $rack);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $em->persist($rack);
            $em->flush();

            return $this->redirectToRoute('rack_index', ['id' => $rack->getId()]);
        }
        $title = 'Rack creation';
        return $this->render('default/new.html.twig', ['entityName' => $this->entity,
                                                       'entity' => $rack,
                                                       'form' => $form->createView(),
                                                       'title' => $title,
                                                       'btn_msg' => 'create']);
    }

    /**
     * @param Request $request
     * @param Rack    $rack
     * @Route("/{id}/edit", name="rack_edit")
     * @Method({"GET", "POST"})
     *
     * @return RedirectResponse|Response
     */
    public function editAction(Request $request, Rack $rack) {
        $editForm = $this->createForm('AppBundle\Form\RackType', $rack);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('rack_index', array('id' => $rack->getId()));
        }

        return $this->render('default/new.html.twig', ['entityName' => $this->entity, 'entity' => $rack, 'form' => $editForm->createView(), 'btn_msg' => 'save', 'title' => 'Rack edit',]);
    }

    /**
     * Finds and displays a rack entity.
     *
     * @Route("/{id}", name="rack_show")
     * @Method("GET")
     */
    public function showAction(Rack $rack) {
        $deleteForm = $this->createDeleteForm($rack);
        $editForm = $this->createEditForm($rack);
        $title = 'Show rack';

        return $this->render('default/show.html.twig', ['entityName' => $this->entity, 'entity' => $rack, 'model' => 'rack', 'title' => $title, 'fields' => Rack::$fields, 'delete_form' => $deleteForm->createView(), 'edit_form' => $editForm->createView(),]);
    }

    /**
     * Deletes a rack entity.
     *
     * @Route("/{id}/delete", name="rack_delete")
     * @Method("GET")
     * @param Request $request
     * @param Rack    $rack
     *
     * @return RedirectResponse
     */
    public function deleteAction(Request $request, Rack $rack) {
        $booksCount = $rack->getBooks()->count();

        if ($booksCount == 0) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($rack);
            $em->flush();
            $response = $this->redirectToRoute('rack_index');
        } else {
            /**@var Translator $trans*/
            $trans =$this->get('translator');

            $message = $trans->trans('errors.rack_contains_books', ['%count%' => $booksCount]);
            $response = $this->forward('AppBundle\Controller\RackController::indexAction', array(
                'error'  => $message
            ));
        }

        return $response;
    }

    /**
     * Creates a form to edit a rack entity.
     *
     * @param Rack $rack The rack entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Rack $rack) {
        return $this->createFormBuilder()->setAction($this->generateUrl('rack_edit', ['id' => $rack->getId()]))->setMethod('POST')->getForm();
    }

    /**
     * Creates a form to delete a rack entity.
     *
     * @param Rack $rack The rack entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Rack $rack) {
        return $this->createFormBuilder()->setAction($this->generateUrl('rack_delete', ['id' => $rack->getId()]))->setMethod('DELETE')->getForm();
    }
}