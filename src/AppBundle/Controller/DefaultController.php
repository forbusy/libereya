<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
	    if($this->getUser()){
		    return $this->render('default/index.html.twig', [
			    'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..'),
		    ]);
	    } else {
		    return $this->render('FOSUserBundle::Security/login.html.twig', [
			    'base_dir' => realpath($this->getParameter('kernel.root_dir').'/../..'),
		    ]);
	    }
    }
}
