<?php

namespace AppBundle\Controller;

use AppBundle\Datatables\BookDatatable;
use AppBundle\Entity\Book;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sg\DatatablesBundle\Datatable\DatatableInterface;
use Doctrine\ORM\QueryBuilder;

/**
 * Book controller.
 *
 * @Route("book")
 */
class BookController extends Controller {
    /**
     * Name of entity for template
     *
     * @var string $entity
     */
    public $entity = 'book';

    /**
     * Lists all book entities.
     *
     * @Route("/", name="book_index")
     * @Method("GET")
     * @param Request $request
     *
     * @return Response
     * @throws \Exception;
     */
    public function indexAction(Request $request) {
        /** @var User $user */
        $user = $this->getUser();
        $isAjax = $request->isXmlHttpRequest();
        /** @var DatatableInterface $datatable */
        $datatable = $this->get('sg_datatables.factory')->create(BookDatatable::class);
        $datatable->buildDatatable();

        if ($isAjax) {
            $responseService = $this->get('sg_datatables.response');
            $responseService->setDatatable($datatable);
            $datatableQueryBuilder = $responseService->getDatatableQueryBuilder();

            /** @var QueryBuilder $qb */
            $qb = $datatableQueryBuilder->getQb();
            $qb->andWhere('book.user = :user');
            $qb->setParameter('user', $user->getId());
            $rack = $request->get('rack');
            if (!empty($rack)) {
                $qb->leftJoin('book.rack', 'rr');
                $qb->andWhere('rr.id = :r');
                $qb->setParameter('r', (int)$rack);
            }

            return $responseService->getResponse();
        }

        return $this->render('default/list_datatable.html.twig', [
            'datatable'  => $datatable,
            'entityName' => $this->entity,
            'model'      => $this->entity,
        ]);
    }

    /**
     * Creates a new book entity.
     *
     * @Route("/new", name="book_new")
     * @Method({"GET", "POST"})
     * @param Request $request
     *
     * @throws
     * @return Response
     */
    public function newAction(Request $request) {
        $fileLoader = $this->container->get('app.files');
        $book = new Book();
        $book->setUser($this->getUser());
        $form = $this->createForm('AppBundle\Form\BookType', $book);

        $form->handleRequest($request);

        if(empty($book->getGroupName()) && ($book->getParts() - $book->getPart()) == 0) {
            $book->setGroupName($book->getName());
        } else {
            $form->get('groupName')->addError(new FormError('For multiple value books this fields is mandatory'));
        }

        if ($form->isSubmitted() && $form->isValid()) {

            $files = $book->getFiles();
            $files->forAll(function($index, $file) use ($book, $fileLoader) {
                return $fileLoader->upload($file, $book);
            });

            /**@var EntityManager $em */
            $em = $this->getDoctrine()->getManager();
            $em->persist($book);
            $em->flush($book);

            return $this->redirectToRoute('book_index');
        }

        return $this->render('default/book.html.twig',
                             [
                                 'entityName' => $this->entity,
                                 'btn_msg'    => 'save',
                                 'book'       => $book,
                                 'form'       => $form->createView()]);
    }

    /**
     * Displays a form to edit an existing book entity.
     *
     * @Route("/{id}", name="book_edit")
     * @Method({"GET", "POST"})
     * @param Request $request
     * @param Book    $book
     *
     * @throws
     * @return Response|RedirectResponse
     */
    public function editAction(Request $request, Book $book) {
        $fileLoader = $this->container->get('app.files');
        $form = $this->createForm('AppBundle\Form\BookType', $book);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $files = $book->getFiles();
            $files->forAll(function($index, $file) use ($book, $fileLoader) {
                return $fileLoader->upload($file, $book);
            });

            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('book_index');
        }

        return $this->render('default/book.html.twig', [
            'entityName' => $this->entity,
            'btn_msg'    => 'save',
            'book'       => $book,
            'form'       => $form->createView()]);
    }

    /**
     * Deletes a book entity.
     *
     * @Route("/{id}/delete", name="book_delete")
     * @Method("GET")
     * @param Book $book
     *
     * @return RedirectResponse
     */
    public function deleteAction(Book $book) {
        $em = $this->getDoctrine()->getManager();
        $em->remove($book);
        $em->flush();

        return $this->redirectToRoute('book_index');
    }
}
