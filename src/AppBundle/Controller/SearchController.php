<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SearchController extends Controller {
    /**
     * @Route("/search", name="search")
     */
    public function search() {

    }

    /**
     * @Route("/author_search", name="author_search")
     * @param Request $request
     * @return Response
     */
    public function searchAuthorAction(Request $request) {
        $q = $request->query->get('term');

        $qb = $this->getDoctrine()->getManager()->createQueryBuilder();
        $result = $qb->select('author')->from('AppBundle\Entity\Author', 'author')
            ->where( $qb->expr()->like('author.name', $qb->expr()->literal('%' . $q . '%')) )
            ->getQuery()
            ->getResult();

        return $this->render('Form/autor_search.html.twig', ['results' => $result]);
    }

    /**
     *
     * @Route("/author_get/{author}", name="author_get")
     * @Method({"GET"})
     * @param int|string $author
     * @return Response
     */
    public function getAuthorAction($author) {
        if((int) $author == 0){
            $author = $this->getDoctrine()->getRepository('AppBundle:Author')->findByName($author)[0];
        } else {
            $author = $this->getDoctrine()->getRepository('AppBundle:Author')->find($author);
        }


        return new Response($author->getName());
    }
}