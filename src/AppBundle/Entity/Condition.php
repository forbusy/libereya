<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="`condition`")
 * @ORM\HasLifecycleCallbacks
 */
class Condition {
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=100, nullable=false)
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Book", mappedBy="condition", cascade={"detach"})
     */
    protected $books;

    /**
     * @return mixed
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Condition
     */
    public function setId($id) {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Condition
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getBooks() {
        return $this->books;
    }

    /**
     * @param ArrayCollection $books
     * @return $this
     */
    public function setBooks(ArrayCollection $books){
        $this->books = $books;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString() {
        return $this->getName();
    }
}