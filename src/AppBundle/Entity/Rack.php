<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="racks")
 */
class Rack {

    public function __construct() {
        $this->books = new ArrayCollection();
    }

    /**
     * Fields for fill
     *
     * @var array
     */
    public static $fields = ['name'];

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @var string
     * @ORM\Column(type="string", length=100, nullable=false)
     */
    protected $name;

    /**
     * @var \AppBundle\Entity\User
     * @ORM\ManyToOne(targetEntity="\AppBundle\Entity\User", inversedBy="racks", cascade={"detach"})
     * @ORM\JoinColumn(name="user", referencedColumnName="id")
     */
    protected $user;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="Book", mappedBy="rack", cascade={"persist"})
     */
    protected $books;

    /**
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param integer $id
     *
     * @return Rack
     */
    public function setId($id) {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return Rack
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * @return \AppBundle\Entity\User
     */
    public function getUser() {

        return $this->user;
    }

    /**
     * @param \AppBundle\Entity\User $user
     *
     * @return $this
     */
    public function setUser(User $user) {
        $this->user = $user;

        return $this;
    }

    /**
     * @param ArrayCollection $books
     *
     * @return $this
     */
    public function setBooks(ArrayCollection $books){
            $books->forAll(function($book){
                $this->addBook($book);
            });

            return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getBooks() {

        return $this->books;
    }

    /**
     * @param Book $book
     *
     * @return $this
     */
    public function removeBook(Book $book) {
        $this->books->removeElement($book);

        return $this;
    }

    /**
     * @param Book $book
     *
     * @return $this
     */
    public function addBook(Book $book) {
        $this->books->add($book);

        return $this;
    }
}