<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @ORM\Entity
 * @ORM\Table(name="files")
 * @ORM\HasLifecycleCallbacks
 */
class File {
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Book
     * @ORM\ManyToOne(targetEntity="Book", inversedBy="files", cascade={"detach"}, fetch="EAGER")
     * @ORM\JoinColumn(name="book_id", referencedColumnName="id")
     */
    private $book;

    /**
     * @var string
     *
     * @ORM\Column(name="name", unique=true, type="string", length=255)
     */
    private $name;

    /**
     * @var UploadedFile
     */
    private $file;

    /**
     * @var string
     * @ORM\Column(name="original_name", type="string", length=255)
     */
    private $originalName;

    /**
     * @return mixed
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param mixed $id
     *
     * @return File
     */
    public function setId($id) {
        $this->id = $id;

        return $this;
    }

    /**
     * @return Book
     */
    public function getBook() {
        return $this->book;
    }

    /**
     * @param Book $book
     *
     * @return File
     */
    public function setBook(Book $book) {
        $this->book = $book;

        return $this;
    }

    /**
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return File
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * @return UploadedFile
     */
    public function getFile() {
        return $this->file;
    }

    /**
     * @param UploadedFile $file
     *
     * @return File
     */
    public function setFile(UploadedFile $file) {
        $this->file = $file;

        return $this;
    }

    /**
     * @return string
     */
    public function getOriginalName() {
        return $this->originalName;
    }

    /**
     * @param string $originalName
     *
     * @return $this
     */
    public function setOriginalName($originalName) {
        $this->originalName = $originalName;

        return $this;
    }

    /**
     * @return string
     */
    public function getExtention() {
        $name = $this->getName();
        $partsArray = explode('.', $name);

        return end($partsArray);
    }

    /**
     * @return bool
     */
    public function isImage() {
        $extentionArray = [
            'jpg',
            'png',
            'jpeg',
            'png',
            'git'
        ];

        return in_array($this->getExtention(), $extentionArray);
    }
}