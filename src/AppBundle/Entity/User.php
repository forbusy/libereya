<?php
namespace AppBundle\Entity;

use Doctrine\ORM\EntityManager;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use AppBundle\EntityTrait\DateTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Event\PreFlushEventArgs;
use Doctrine\ORM\EntityRepository;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * User class
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="users")
 *
 */
class User extends BaseUser implements UserInterface {
    use DateTrait;

    const ROLE_USER = 'ROLE_USER';
    const ROLE_ADMIN = 'ROLE_ADMIN';

    /**
     * @var array
     */
    public static $roleNames = [self::ROLE_USER, self::ROLE_ADMIN];

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="Role", cascade={"detach", "persist"}, inversedBy="users")
     * @ORM\JoinTable(name="user_role")
     */
    protected $roles;
    /**
     * @var string
     * @ORM\Column(type="string", name="full_name", nullable=true)
     */
    private $fullName;
    /**
     * @var array
     */
    private $rolesBuffer;

    /**
     * @var string
     * @ORM\Column(type="string", name="country", nullable=true)
     */
    private $country;

    /**
     * @Assert\NotBlank(groups={"Profile", "ResetPassword", "Registration", "ChangePassword"})
     * @Assert\Length(min=3, max=100, minMessage="user.password.short", groups={"Profile", "ResetPassword", "Registration", "ChangePassword"})
     * @Assert\Regex(pattern="/(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9]).{6,}/",
     *  message="Password must be six or more characters long and contain at least one digit, one upper- and one lowercase character.",
     * groups={"Profile", "ResetPassword", "Registration", "ChangePassword"})
     */
    protected $plainPassword;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Book", mappedBy="user", cascade={"detach"})
     */
    protected $books;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Rack", mappedBy="user", cascade={"detach"})
     */
    protected $racks;


    /**
     * @inheritdoc
     */
    public function __construct() {
        parent::__construct();
        $this->roles = new ArrayCollection();
    }

    /**
     * After load data
     *
     * @ORM\PostLoad
     */
    public function afterLoad() {
        $this->rolesBuffer = [];

        foreach($this->roles as $role) {
            $this->rolesBuffer[] = $role->getName();
        }
    }

    /**
     * Pre Flush Event
     *
     * @ORM\PreFlush
     * @param PreFlushEventArgs $preFlushEventArgs
     */
    public function beforeFlush(PreFlushEventArgs $preFlushEventArgs) {
        /** @var EntityManager $em */
        $em = $preFlushEventArgs->getEntityManager();

        /** @var $roleRepository EntityRepository */
        $roleRepository = $em->getRepository('AppBundle:Role');

        $this->roles->clear();

        foreach($this->getRoles() as $roleName) {
            $this->roles->add($roleRepository->findOneByName($roleName));
        }
    }

    /**
     * Get user roles
     *
     * @return array
     */
    public function getRoles() {
        if(isset($this->rolesBuffer) && is_array($this->rolesBuffer)) {
            return $this->rolesBuffer;
        }

        $this->rolesBuffer = [];

        foreach($this->roles as $role) {
            $this->rolesBuffer[] = $role->getName();
        }

        return $this->rolesBuffer;
    }

    /**
     * Set user roles
     *
     * @param array $roles
     * @return User
     */
    public function setRoles(array $roles) {
        $this->rolesBuffer = [];

        foreach($roles as $role) {
            $this->addRole($role);
        }

        return $this;
    }

    /**
     * Add user role
     *
     * @param string $role
     * @return $this
     */
    public function addRole($role) {
        $role = strtoupper($role);

        if(!in_array($role, $this->getRoles(), true)) {
            $this->rolesBuffer[] = $role;
        }

        return $this;
    }

    /**
     * Get fullName
     *
     * @return string
     */
    public function getFullName() {
        return $this->fullName;
    }

    /**
     * Set fullName
     *
     * @param string $fullName
     * @return User
     */
    public function setFullName($fullName) {
        $this->fullName = $fullName;

        return $this;
    }

    /**
     * Remove user role
     *
     * @param string $role
     * @return $this
     */
    public function removeRole($role) {
        if(false !== $key = array_search(strtoupper($role), $this->getRoles(), true)) {
            unset($this->rolesBuffer[$key]);
            $this->rolesBuffer = array_values($this->rolesBuffer);
        }

        return $this;
    }

    /**
     * Check has user role
     *
     * @param string $role
     * @return bool
     */
    public function hasRole($role) {
        return in_array(strtoupper($role), $this->getRoles(), true);
    }

    /**
     * Get country
     * @return string $country
     */
    public function getCountry() {
        return $this->country;
    }

    /**
     * Set country
     * @param string $country
     * @return $this
     */
    public function setCountry($country) {
        $this->country = $country;

        return $this;
    }

    /**
     * Convert entity to string
     *
     * @return string
     */
    public function __toString() {
        return $this->username ?: 'Create User';
    }

    /**
     * @return ArrayCollection
     */
    public function getBooks() {
        return $this->books;
    }

    /**
     * @param ArrayCollection $books
     * @return $this
     */
    public function setBooks(ArrayCollection $books) {
        $this->books = $books;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getRacks() {
        return $this->racks;
    }

    /**
     * @param ArrayCollection $racks
     * @return $this
     */
    public function setRacks(ArrayCollection $racks) {
        $this->racks = $racks;

        return $this;
    }

}