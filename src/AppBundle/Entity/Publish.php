<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="publishers")
 */
class Publish {
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=100, nullable=false)
     * @Assert\NotBlank()
     */
    protected $name;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    protected $description;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Book", mappedBy="publish", cascade={"detach"})
     */
    protected $books;

    /**
     * @inheritdoc
     */
    public function __construct() {
        $this->books = new ArrayCollection();
    }

    /**
     * Get Publish name
     *
     * @return string
     */
    public function __toString() {
        return (string)$this->name;
    }


    /**
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Publish
     */
    public function setName($name) {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Publish
     */
    public function setDescription($description) {
        $this->description = $description;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getBooks() {
        return $this->books;
    }

    /**
     * @param ArrayCollection $books
     * @return Publish
     */
    public function setBooks(ArrayCollection $books) {
        $this->books = $books;

        return $this;
    }

    /**
     * Add book
     *
     * @param Book $book
     * @return Publish
     */
    public function addBook(Book $book) {
        if(!$this->books->contains($book)) {
            $this->books->add($book);
        }

        return $this;
    }
}