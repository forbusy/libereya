<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="books")
 * @ORM\HasLifecycleCallbacks
 */
class Book {
    public static $fields = ['name', 'year', 'author', 'publish'];

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    protected $isbn;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="books", cascade={"detach"})
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * @var string
     * @ORM\Column(type="string", length=512, nullable=false)
     * @Assert\NotBlank()
     */
    protected $groupName;

    /**
     * @var string
     * @ORM\Column(type="string", length=512, nullable=false)
     * @Assert\NotBlank()
     */
    protected $name;

    /**
     * @var string
     * @ORM\Column(type="string", length=4, nullable=true)
     */
    protected $year;

    /**
     * @var Author
     * @ORM\ManyToOne(targetEntity="Author", inversedBy="books", cascade={"detach"}, fetch="EAGER")
     * @ORM\JoinColumn(name="author_id", referencedColumnName="id")
     */
    protected $author;

    /**
     * @var Cover
     * @ORM\ManyToOne(targetEntity="Cover", inversedBy="books", cascade={"detach"}, fetch="EAGER")
     * @ORM\JoinColumn(name="cover_id", referencedColumnName="id")
     */
    protected $cover;

    /**
     * @var int
     * @ORM\Column(type="integer", length=5, nullable=true)
     */
    protected $price;

    /**
     * @var Publish
     * @ORM\ManyToOne(targetEntity="Publish", inversedBy="books", cascade={"detach"}, fetch="EAGER")
     * @ORM\JoinColumn(name="publish_id", referencedColumnName="id")
     */
    protected $publish;

    /**
     * @var int
     * @ORM\Column(type="integer", length=5, nullable=true)
     */
    protected $pages;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    protected $description;

    /**
     * @var Condition
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Condition", inversedBy="books", cascade={"detach"}, fetch="EAGER")
     * @ORM\JoinColumn(name="condition_id", referencedColumnName="id")
     */
    protected $condition;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="Rack", inversedBy="books", cascade={"detach"}, fetch="EAGER")
     * @ORM\JoinTable(name="books_racks",
     *     joinColumns={@ORM\JoinColumn(name="book_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="rack_id", referencedColumnName="id")})
     */
    protected $rack;

    /**
     * @var Genre
     * @ORM\ManyToOne(targetEntity="Genre", inversedBy="books", cascade={"detach"}, fetch="EAGER")
     * @ORM\JoinColumn(name="genre_id",  referencedColumnName="id")
     */
    protected $genre;

    /**
     * @var bool
     * @ORM\Column(type="boolean")
     */
    protected $illustrated;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    public $print;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="File", mappedBy="book", cascade={"persist", "remove"})
     */
    private $files;

    /**
     * @var bool
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $wish;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true, length=3)
     */
    protected $part;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true, length=3)
     */
    protected $parts;

    /**
     * Book constructor.
     */
    public function __construct() {
        $this->rack = new ArrayCollection();
        $this->files = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getPart() {
        return $this->part;
    }

    /**
     * @param int $part
     *
     * @return Book
     */
    public function setPart($part) {
        $this->part = $part;

        return $this;
    }

    /**
     * @return int
     */
    public function getParts() {
        return $this->parts;
    }

    /**
     * @param int $parts
     *
     * @return Book
     */
    public function setParts($parts) {
        $this->parts = $parts;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getFiles() {
        return $this->files;
    }

    /**
     * @param ArrayCollection $files
     *
     * @return $this
     */
    public function setFiles(ArrayCollection $files) {
        $this->files = $files;

        return $this;
    }

    /**
     * @param File $file
     *
     * @return $this
     */
    public function addFile(File $file) {
        $this->files->add($file);

        return $this;
    }

    /**
     * @param File $file
     *
     * @return $this
     */
    public function removeFile(File $file) {
        $this->files->removeElement($file);

        return $this;
    }

    /**
     * @param $file
     *
     * @return bool
     */
    public function hasFile($file) {
        return $this->files->contains($file);
    }

    /**
     * @return $this
     */
    public function clearFiles() {
        $this->files = new ArrayCollection();

        return $this;
    }

    /**
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return Book
     */
    public function setId($id) {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return Book
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getGroupName() {
        return $this->groupName;
    }

    /**
     * @param string $value
     * @return Book
     */
    public function setGroupName($value) {
        $this->groupName = $value;

        return $this;
    }

    /**
     * @return string
     */
    public function getYear() {
        return $this->year;
    }

    /**
     * @param string $year
     *
     * @return Book
     */
    public function setYear($year) {
        $this->year = $year;

        return $this;
    }

    /**
     * @return Author
     */
    public function getAuthor() {
        return $this->author;
    }

    /**
     * @param \AppBundle\Entity\Author $author
     *
     * @return Book
     */
    public function setAuthor($author) {
        $this->author = $author;

        return $this;
    }

    /**
     * @return Cover
     */
    public function getCover() {
        return $this->cover;
    }

    /**
     * @param Cover $cover
     *
     * @return Book
     */
    public function setCover($cover) {
        $this->cover = $cover;

        return $this;
    }

    /**
     * @return int
     */
    public function getPrice() {
        return $this->price;
    }

    /**
     * @param int $price
     *
     * @return Book
     */
    public function setPrice($price) {
        $this->price = $price;

        return $this;
    }

    /**
     * @return \AppBundle\Entity\Publish
     */
    public function getPublish() {
        return $this->publish;
    }

    /**
     * @param \AppBundle\Entity\Publish $publish
     *
     * @return Book
     */
    public function setPublish($publish) {
        $this->publish = $publish;

        return $this;
    }

    /**
     * @return int
     */
    public function getPages() {
        return $this->pages;
    }

    /**
     * @param int $pages
     *
     * @return Book
     */
    public function setPages($pages) {
        $this->pages = $pages;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * @param string $description
     *
     * @return Book
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Condition
     */
    public function getCondition() {
        return $this->condition;
    }

    /**
     * @param Condition $condition
     *
     * @return Book
     */
    public function setCondition(Condition $condition) {
        $this->condition = $condition;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getRack() {
        return $this->rack->getValues();
    }

    /**
     * @param ArrayCollection $racks
     *
     * @return Book
     */
    public function setRack(ArrayCollection $racks = null) {
        $racks->forAll(function($rack) {
            $this->addRack($rack);
        });

        return $this;
    }

    /**
     * @param Rack $rack
     *
     * @return $this
     */
    public function removeRack(Rack $rack) {
        $this->rack->removeElement($rack);

        return $this;
    }

    /**
     * @param Rack $rack
     *
     * @return $this
     */
    public function addRack(Rack $rack) {
        $this->rack->add($rack);

        return $this;
    }

    /**
     * @return boolean
     */
    public function isIllustrated() {
        return $this->illustrated;
    }

    /**
     * @param boolean $illustrated
     *
     * @return Book
     */
    public function setIllustrated($illustrated) {
        $this->illustrated = $illustrated;

        return $this;
    }

    /**
     * @return Genre
     */
    public function getGenre() {
        return $this->genre;
    }

    /**
     * @param Genre $genre
     *
     * @return Book
     */
    public function setGenre($genre) {
        $this->genre = $genre;

        return $this;
    }

    /**
     * @return User
     */
    public function getUser() {
        return $this->user;
    }

    /**
     * @param User $user
     *
     * @return $this
     */
    public function setUser(User $user) {
        $this->user = $user;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getIsbn() {
        return $this->isbn;
    }

    /**
     * @param string $isbn
     *
     * @return Book
     */
    public function setIsbn($isbn) {
        $this->isbn = $isbn;

        return $this;
    }

    /**
     * @return bool
     */
    public function getWish() {
        return $this->wish;
    }

    /**
     * @param bool $wish
     *
     * @return Book
     */
    public function setWish($wish) {
        $this->wish = $wish;

        return $this;
    }

    /**
     * @return string
     */
    public function getPrint() {
        return $this->print;
    }

    /**
     * @param string $print
     *
     * @return Book
     */
    public function setPrint($print) {
        $this->print = $print;

        return $this;
    }
}