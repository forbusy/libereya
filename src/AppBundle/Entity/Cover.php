<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="covers")
 */
class Cover {
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=100, nullable=false)
     * @Assert\NotBlank()
     */
    protected $name;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Book", mappedBy="cover", cascade={"detach"})
     */
    protected $books;

    /**
     * @inheritdoc
     */
    public function __construct() {
        $this->books = new ArrayCollection();
    }

    /**
     * @return integer $id
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param integer $id
     * @return Cover
     */
    public function setId($id) {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Cover
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getBooks() {
        return $this->books;
    }

    /**
     * @param ArrayCollection $books
     * @return Cover
     */
    public function setBooks(ArrayCollection $books) {
        $this->books = $books;

        return $this;
    }

    /**
     * Add book
     *
     * @param Book $book
     * @return Cover
     */
    public function addBook(Book $book) {
        if(!$this->books->contains($book)) {
            $this->books->add($book);
        }

        return $this;
    }

    /**
     * @return string
     */
    public function __toString() {
        return $this->getName();
    }

}