<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Translation\Translator;

class UserType extends AbstractType {

	/**
	 * Form
	 *
	 * @param FormBuilderInterface $builder
	 * @param array                $options
	 */
	public function buildForm(FormBuilderInterface $builder, array $options){

		$builder->add('fullName', TextType::class,
			['attr' => ['class' => 'form-control', 'placeholder'=>'registration.full_name', 'autofocus' => true], 'label' => false])
                ->add('userName', TextType::class,
					['attr' => ['class' => 'form-control', 'placeholder' => 'registration.user_name'], 'label' => false])
                ->add('email', EmailType::class,
					['attr' => ['class' => 'form-control', 'placeholder' => 'email'], 'label' => false])
                ->add('country','country',
					['attr' => ['class' => 'form-control', 'placeholder' =>'Country'], 'label' => false])
                ->add('plainPassword', RepeatedType::class,
					['attr' => ['class' => 'form-control'], 'type' => PasswordType::class, 'first_options' => ['label' => false, 'attr' => ['placeholder' => 'registration.password']], 'second_options' => ['label' => false, 'attr' => ['placeholder' => 'registration.repeat_password']]]);
	}

	/**
	 * {@inheritdoc}
	 */
	public function configureOptions(OptionsResolver $resolver){
		$resolver->setDefaults(['data_class' => 'AppBundle\Entity\User']);
	}

	/**
	 * @return string
	 */
	public function getName(){
		return 'fos_user.registration.form.handler';
	}
}