<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;

class ResettingFormType extends AbstractType
{
    /**
     * Form
     *
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options){
        $builder->add('plainPassword', RepeatedType::class, [
            'attr' => ['class' => 'form-control'],
            'type' => PasswordType::class,
            'first_options' => [
                'label' => false,
                'attr' => [
                    'placeholder' => 'Password']],
            'second_options' => [
                'label' => false,
                'attr' => [
                    'placeholder' => 'Repeat password']]]);
    }

    /**
     * {@inheritdoc}
     */
  /*  public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => $this->class,
            'csrf_token_id' => 'resetting',
            // BC for SF < 2.8
            'intention' => 'resetting',
        ));
    }
*/
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults(['data_class' => 'AppBundle\Entity\User']);
    }

    // BC for SF < 3.0
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->getBlockPrefix();
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'fos_user_resetting';
    }
}
