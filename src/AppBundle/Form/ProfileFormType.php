<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Translation\Translator;

class ProfileFormType extends AbstractType {
    /**
     * @var string
     */
    private $class;

    /**
     * @param string $class The User class name
     */
    public function __construct($class) {
        $this->class = $class;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $this->buildUserForm($builder, $options);

        $constraintsOptions = array('message' => 'fos_user.current_password.invalid',);

        if(!empty($options['validation_groups'])) {
            $constraintsOptions['groups'] = array(reset($options['validation_groups']));
        }

        /*$builder->add('current_password', PasswordType::class, ['attr' => ['class'=>'form-group', 'label' => 'form.current_password',
            'translation_domain' => 'FOSUserBundle',
            'mapped' => false,
            'constraints' => new UserPassword($constraintsOptions)]]);*/
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults([
            'data_class' => $this->class,
            'csrf_token_id' => 'profile', // BC for SF < 2.8
            'intention' => 'profile'
        ]);
    }

    // BC for SF < 3.0

    /**
     * {@inheritdoc}
     */
    public function getName() {
        return $this->getBlockPrefix();
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {
        return 'fos_user_profile';
    }

    /**
     * Builds the embedded form representing the user.
     *
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    protected function buildUserForm(FormBuilderInterface $builder, array $options) {
        /**@var Translator $trans*/
        $trans = new Translator('en-EN');

        $builder->add('userName', TextType::class, ['attr' => ['class'=>'form-group', 'label' => $trans->trans('profile.edit.username', [], 'FOSUserBundle')], 'label_attr' => ['class' => 'control-label col-sm-2']])
            ->add('email', EmailType::class, ['attr' => ['class'=>'form-group', 'label' => $trans->trans('form.email', [], 'FOSUserBundle')], 'label_attr' => ['class' => 'control-label col-sm-2']])
            ->add('fullName', TextType::class, ['attr' => ['class'=>'form-group', 'label' => $trans->trans('profile.edit.fullname', [], 'FOSUserBundle')], 'label_attr' => ['class' => 'control-label col-sm-2']])
            ->add('country', CountryType::class, ['attr' => ['class'=>'form-group', 'label' => $trans->trans('profile.edit.country', [], 'FOSUserBundle')], 'label_attr' => ['class' => 'control-label col-sm-2']]);
    }
}
