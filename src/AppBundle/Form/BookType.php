<?php

namespace AppBundle\Form;

use AppBundle\Entity\Author;
use AppBundle\Entity\Condition;
use AppBundle\Entity\File;
use AppBundle\Entity\Genre;
use AppBundle\Entity\Publish;
use AppBundle\Form\Type\DataListType;
use Doctrine\ORM\EntityManager;
use PUGX\AutocompleterBundle\Form\Type\AutocompleteType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Translation\Translator;
use AppBundle\Entity\Book;

class BookType extends AbstractType {

    /**@var EntityManager */
    private $em;

    /**@var RequestStack */
    private $rq;

    /**@var Translator */
    private $translator;

    public function __construct(EntityManager $em, RequestStack $requestStack, Translator $translator) {
        $this->em = $em;
        $this->rq = $requestStack;
        $this->translator = $translator;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        /**@var Translator $trans */
        $trans = $this->translator;

        $builder->add('name', TextType::class, [
            'attr'       => [
                'class' => 'form-control',
                'label' => $trans->trans('forms.books.name')],
            'label_attr' => ['class' => 'control-label']])
            ->add('isbn', TextType::class, [
                'required'   => false,
                'attr'       => [
                    'class' => 'form-control',
                    'label' => $trans->trans('forms.books.isbn')],
                'label_attr' => ['class' => 'control-label']])
            ->add('year', TextType::class, [
                'required'   => false,
                'attr'       => [
                    'class' => 'form-control',
                    'label' => $trans->trans('forms.books.year')],
                'label_attr' => ['class' => 'control-label']])
            ->add('price', NumberType::class, [
                'required'   => false,
                'attr'       => [
                    'class' => 'form-control',
                    'label' => $trans->trans('forms.books.price')],
                'label_attr' => ['class' => 'control-label']])
            ->add('pages', IntegerType::class, [
                'required'   => false, 'attr' => [
                    'class' => 'form-control',
                    'label' => $trans->trans('forms.books.pages')],
                'label_attr' => ['class' => 'control-label']])
            ->add('description', TextareaType::class, [
                'required'   => false,
                'attr'       => [
                    'class' => 'form-control',
                    'label' => $trans->trans('forms.books.description')],
                'label_attr' => ['class' => 'control-label']])
            ->add('illustrated', CheckboxType::class, [
                'required'   => false,
                'attr'       => [
                    'class' => 'form-check-input',
                    'label' => $trans->trans('forms.books.illustrated')],
                'label_attr' => ['class' => 'form-check-label']])
            ->add('author', AutocompleteType::class, [
                'class'      => 'AppBundle:Author',
                'required'   => false,
                'attr'       => [
                    'class' => 'form-control',
                    'label' => $trans->trans('forms.books.author'),
                    'value' => $this->getAuthorValue($options['data'])],
                'label_attr' => ['class' => 'control-label']])
            ->add('cover', EntityType::class, [
                'required'     => false,
                'class'        => 'AppBundle:Cover',
                'choice_label' => 'name',
                'choice_value' => 'id',
                'attr'         => [
                    'class' => 'form-control',
                    'label' => $trans->trans('forms.books.cover')],
                'label_attr'   => ['class' => 'control-label']])
            ->add('publish', DataListType::class, [
                'required'   => false,
                'choices'    => $this->getDataForPublish(),
                'id'         => 'publish_field_id',
                'data_class' => Publish::class,
                'attr'       => [
                    'class' => 'form-control', 'label' => $trans->trans('forms.books.publish'),
                    'value' => $this->getValueForPublish($options['data'])],
                'label_attr' => ['class' => 'control-label']])
            ->add('condition', DataListType::class, [
                'required'   => false,
                'choices'    => $this->getDataForCondition(),
                'id'         => 'condition_field_id',
                'data_class' => Condition::class,
                'attr'       => [
                    'class' => 'form-control',
                    'label' => $trans->trans('forms.books.condition'),

                    'value' => $this->getValueForCondition($options['data'])],
                'label_attr' => ['class' => 'control-label']])
            ->add('rack', EntityType::class, [
                'required'     => false,
                'multiple'     => true,
                'class'        => 'AppBundle\Entity\Rack',
                'choice_label' => 'name',
                'attr'         => [
                    'class' => 'form-control',
                    'label' => $trans->trans('forms.books.rack')],
                'label_attr'   => ['class' => 'control-label']])
            ->add('genre', DataListType::class, [
                'required'   => false,
                'choices'    => $this->getDataForGenre(),
                'id'         => 'genre_field_id',
                'data_class' => Genre::class,
                'attr'       => [
                    'class' => 'form-control',
                    'label' => $trans->trans('forms.books.genre'),
                    'value' => $this->getValueForGenre($options['data'])],
                'label_attr' => ['class' => 'control-label']])
            ->add('wish', CheckboxType::class, [
                'required'   => false,
                'attr'       => [
                    'class' => 'form-check-input',
                    'label' => $trans->trans('forms.books.wish')],
                'label_attr' => ['class' => 'form-check-label']])
            ->add('part', NumberType::class, [
                'required'   => false, 'attr' => [
                    'class' => 'form-control',
                    'label' => $trans->trans('forms.books.part')],
                'label_attr' => ['class' => 'control-label']])
            ->add('parts', NumberType::class, [
                'required'   => false,
                'attr'       => [
                    'class' => 'form-control',
                    'label' => $trans->trans('forms.books.parts')],
                'label_attr' => ['class' => 'control-label']])
            ->add('print', TextType::class, [
                'required'   => false,
                'attr'       => [
                    'class' => 'form-control',
                    'label' => $trans->trans('forms.books.print')],
                'label_attr' => ['class' => 'control-label']])
            ->add('files', 'file', ['required' => false, 'multiple' => true, 'data_class' => null])
            ->add('groupName', TextType::class, [
                'required'   => false,
                'attr'       => [
                    'class' => 'form-control',
                    'label' => $trans->trans('forms.books.group_name')
                ],
                'label_attr' => ['class' => 'control-label']
            ])
            ->add('save', SubmitType::class, ['attr' => ['class' => 'btn btn-primary icon-save', 'label' => 'Save']]);


        $builder->get('publish')->addModelTransformer(new CallbackTransformer(function($model) {
            return $model;
        }, function($value) {
            $model = $this->em->getRepository('AppBundle:Publish')->findOneBy(['name' => $value]);
            if (empty($model)) {
                $model = $this->makeModel('AppBundle\Entity\Publish', $value);
            }
            return $model;
        }));

        $builder->get('cover')->addModelTransformer(new CallbackTransformer(function($model) {
            return $model;
        }, function($value) {
            if ($value instanceof \AppBundle\Entity\Cover) return $value;
            $model = $this->em->getRepository('AppBundle:Cover')->findOneBy(['name' => $value]);
            if (empty($model)) {
                $model = $this->makeModel('AppBundle\Entity\Cover', $value);
            }

            return $model;
        }));

        $builder->get('condition')->addModelTransformer(new CallbackTransformer(function($model) {
            return $model;
        }, function($value) {
            $model = $this->em->getRepository('AppBundle:Condition')->findOneBy(['name' => $value]);
            if (empty($model)) {
                $model = $this->makeModel('AppBundle\Entity\Condition', $value);
            }
            return $model;
        }));

        $builder->get('author')->addModelTransformer(new CallbackTransformer(function($model) {
            if (is_int($model)) {
                $model = $this->em->getRepository('AppBundle:Author')->find($model);
                $request = $this->rq->getCurrentRequest()->get('fake_appbundle_book');
                $author = $request['author'];
                if (!empty($model) && !empty($author) && $author != $model->getName()) {
                    $model = $this->em->getRepository('AppBundle:Author')->findOneBy(['name' => $author]);
                    if (empty($model)) {
                        $model = $this->makeModel('AppBundle\Entity\Author', $author);
                    }
                }

            }
            if (empty($model)) {
                $request = $this->rq->getCurrentRequest()->get('fake_appbundle_book');
                $author = $request['author'];
                if (!empty($author)) {
                    $model = $this->em->getRepository('AppBundle:Author')->findOneBy(['name' => $author]);
                }

                if (empty($model) && !empty($author)) {
                    $model = $this->makeModel('AppBundle\Entity\Author', $author);
                }
            }

            return $model;
        }, function($value) {
            $fakeValue = $this->rq->getCurrentRequest()->get('fake_appbundle_book')['author'];

            if (empty($value) || $value != $fakeValue) {
                $value = $fakeValue;
            }
            $model = $this->em->getRepository('AppBundle:Author')->findOneBy(['name' => $value]);
            if (empty($model)) {
                $model = $this->makeModel(' AppBundle\Entity\Author', $value);
            }
            return $model;
        }), true);

        $builder->get('genre')->addModelTransformer(new CallbackTransformer(function($model) {
            return $model;
        }, function($value) {
            $model = $this->em->getRepository('AppBundle:Genre')->findOneBy(['name' => $value]);
            if (empty($model)) {
                $model = $this->makeModel(' AppBundle\Entity\Genre', $value);
            }
            return $model;
        }));

        $builder->get('files')->addModelTransformer(new CallbackTransformer(function($model) {
            return $model;
        }, function($value) {
            /**@var UploadedFile $item */
            foreach ($value as &$item) {
                $file = new File();
                $file->setFile($item);
                $item = $file;
            }

            return $value;
        }));
    }

    /**
     * @param $modelName
     * @param $value
     *
     * @return Author|Condition|Genre|Publish
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function makeModel($modelName, $value) {
        /** @var Author | Genre | Condition | Publish $model */
        $model = new $modelName();
        $model->setName($value);
        $this->em->persist($model);
        $this->em->flush($model);

        return $model;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(['data_class' => 'AppBundle\Entity\Book', 'compound' => true]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {
        return 'appbundle_book';
    }

    /**
     * @return \AppBundle\Entity\Condition[]|array
     */
    private function getDataForCondition() {
        $repository = $this->em->getRepository('AppBundle:Condition');

        return $repository->findAll();
    }

    /**
     * @param Book $book
     *
     * @return string
     */
    private function getValueForCondition(Book $book) {
        if ($book->getCondition()) {
            return $book->getCondition()->getName();
        }

        return '';
    }

    /**
     * @return array
     */
    private function getDataForPublish() {
        $repository = $this->em->getRepository('AppBundle:Publish');

        return $repository->findAll();
    }

    /**
     * @param Book $book
     *
     * @return string
     */
    private function getAuthorValue(Book $book) {
        if ($book->getAuthor()) {
            return $book->getAuthor()->getName();
        }

        return '';
    }

    /**
     * @param Book $book
     *
     * @return string
     */
    private function getValueForPublish(Book $book) {
        if ($book->getPublish()) {
            return $book->getPublish()->getName();
        }

        return '';
    }

    /**
     * @return array
     */
    private function getDataForGenre() {
        $repository = $this->em->getRepository('AppBundle:Genre');

        return $repository->findAll();
    }

    /**
     * @param Book $book
     *
     * @return string
     */
    private function getValueForGenre(Book $book) {

        if ($book->getGenre()) {
            return $book->getGenre()->getName();
        }

        return '';
    }

    /**
     * @return mixed
     */
    public function getParent() {
        return TextType::class;
    }

    /**
     * @return string
     */
    public function getName() {
        return 'app.book.form.handler';
    }
}
