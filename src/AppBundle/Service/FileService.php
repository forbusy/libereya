<?php

namespace AppBundle\Service;

use AppBundle\Entity\File;
use AppBundle\Entity\Book;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Security\Acl\Exception\Exception;

class FileService {

    private $targetDirectory;

    public function __construct($targetDirectory) {
        $this->targetDirectory = $targetDirectory;
    }

    public function upload(File $file, Book $book) {
        $fileName = md5(uniqid()) . '.' . $file->getFile()->guessExtension();

        $file->getFile()->move($this->getTargetDirectory(), $fileName);
        $file->setBook($book);
        $file->setName($fileName);
        $file->setOriginalName($file->getFile()->getClientOriginalName());

        return $fileName;
    }

    public function getTargetDirectory() {
        return $this->targetDirectory;
    }

    /**
     * @param File $file
     *
     * @return \Exception|Exception
     */
    public function delete(File $file) {
        $fileSystem = new Filesystem();
        $dir = $this->getTargetDirectory();

        try{
            $fileSystem->remove($dir.'/'.$file->getName());
        } catch (Exception $e) {
            return $e;
        }
    }
}