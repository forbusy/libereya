<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Cover;

class CoverData extends AbstractFixture implements FixtureInterface {

    /**
     * @return array
     */
    public function getCovers() {
        return ['Отсутствует', 'Лидерин', 'Кожа', 'Полукожа', 'Суперобложка', 'Коленкор', 'Мягкая',];
    }

    /**
     * Load data fixtures
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager) {
        $repository = $manager->getRepository('AppBundle:Cover');
        $covers = $this->getCovers();

        $coverArray = [];
        foreach($covers as $cov) {
            $coverArray[$cov] = $repository->findBy(['name' => $cov]);
            if(count($coverArray[$cov]) == 0) {
                $cover = new Cover();
                $cover->setName($cov);
                $manager->persist($cover);
            }
        }
        $manager->flush();
    }
}