<?php

namespace Appundle\DataFixtures\ORM;

use AppBundle\Entity\User;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;

class UserData extends AbstractFixture implements FixtureInterface, DependentFixtureInterface
{
    /**
     * Load data fixtures with the passed EntityManager
     * @throws
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $userAdmin = $manager->getRepository('AppBundle:User')->findOneByUsername('admin');
        if (empty($userAdmin)) {
            $userAdmin = new User();
            $userAdmin->setFullName('Administrator');
            $userAdmin->setUsername('admin');
            $userAdmin->setPlainPassword('admin_123');
            $userAdmin->setEmail('posmitniy.andrey@gmail.com');
            $userAdmin->setRoles([User::ROLE_ADMIN]);
            $userAdmin->setCountry('UA');
            $userAdmin->setEnabled(true);
            $manager->persist($userAdmin);
        }
        //Share this fixtures for other
        $this->addReference('userAdmin', $userAdmin);

        $user = $manager->getRepository('AppBundle:User')->findOneBy(['email' => 'forbusy@yahoo.com']);
        if(empty($user)) {
            $user = new User();
            $user->setFullName('Andrey Posmitny');
            $user->setCountry('UA');
            $user->setUsername('forbusy');
            $user->setRoles([User::ROLE_USER]);
            $user->setPlainPassword('salo99');
            $user->setEnabled(true);
            $user->setEmail('forbusy@yahoo.com');
            $manager->persist($user);
        }

        $manager->flush();
    }

    /**
     * @inheritdoc
     */
    public function getDependencies()
    {
        return [
            'AppBundle\DataFixtures\ORM\RoleData',
            'AppBundle\DataFixtures\ORM\RoleData',
        ];
    }
}