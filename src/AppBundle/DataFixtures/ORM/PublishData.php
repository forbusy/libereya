<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Publish;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;

class PublishData extends AbstractFixture implements FixtureInterface {

    /**
     * @return array
     */
    private function getPublish() {
        return ['Сытина', 'Вольфа', 'Маркса', 'Девриена', 'Кульженко', 'Кнебеля', 'Экспедиции заготовления ценных бумаг', 'Брокгауза и Ефрона', 'Смирдина', 'Семена Августа', 'Плюшара', 'Селивановского', 'Кнебеля', 'Просвещение', 'Московская типография', 'Типография академии наук', 'Университетская типография',];
    }

    /**
     * Load data fixures
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager) {
        $publishers = $this->getPublish();
        $repository = $manager->getRepository('AppBundle:Publish');
        $pub = [];
        foreach($publishers as $publisher) {
            $pub[$publisher] = $repository->findBy(['name' => $publisher]);

            if(count($pub[$publisher])==0) {
                $publish = new Publish();
                $publish->setName($publisher);
                $manager->persist($publish);
            }
        }

        $manager->flush();

    }
}