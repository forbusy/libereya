<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use AppBundle\Entity\Genre;

class GenreData extends AbstractFixture implements FixtureInterface
{

    /**
     * Get genres
     * @return array
     */
    private function getGenre() 
    {
        return [
            'История','Право', 'Роман', 'Энциклопедия', 'Религия', 'Путешествия', 'Биографии', 'Стихотворения'
        ];
    }
    
    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $genres = $this->getGenre();

        foreach($genres as $gnr){
            $genre = new Genre();
            $genre->setName($gnr);
            $manager->persist($genre);
        }

        $manager->flush();
    }

}