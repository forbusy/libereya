<?php

namespace AppBundle\Datatables;

use Sg\DatatablesBundle\Datatable\AbstractDatatable;
use Sg\DatatablesBundle\Datatable\Style;
use Sg\DatatablesBundle\Datatable\Column\Column;
use Sg\DatatablesBundle\Datatable\Column\BooleanColumn;
use Sg\DatatablesBundle\Datatable\Column\ActionColumn;
use Sg\DatatablesBundle\Datatable\Column\MultiselectColumn;
use Sg\DatatablesBundle\Datatable\Column\VirtualColumn;
use Sg\DatatablesBundle\Datatable\Column\DateTimeColumn;
use Sg\DatatablesBundle\Datatable\Column\ImageColumn;
use Sg\DatatablesBundle\Datatable\Filter\TextFilter;
use Sg\DatatablesBundle\Datatable\Filter\NumberFilter;
use Sg\DatatablesBundle\Datatable\Filter\SelectFilter;
use Sg\DatatablesBundle\Datatable\Filter\DateRangeFilter;
use Sg\DatatablesBundle\Datatable\Editable\CombodateEditable;
use Sg\DatatablesBundle\Datatable\Editable\SelectEditable;
use Sg\DatatablesBundle\Datatable\Editable\TextareaEditable;
use Sg\DatatablesBundle\Datatable\Editable\TextEditable;

/**
 * Class BookDatatable
 *
 * @package AppBundle\Datatables
 */
class BookDatatable extends AbstractDatatable
{
    /**
     * {@inheritdoc}
     */
    public function buildDatatable(array $options = [])
    {
        $this->language->set([
            'cdn_language_by_locale' => true
            //'language' => 'de'
        ]);

        $this->ajax->set([]);

        $this->options->set([
            'individual_filtering' => false,
            'individual_filtering_position' => 'head',
            'order_cells_top' => true,
        ]);

        $this->features->set([]);

        $this->columnBuilder
            ->add('name', Column::class, [
                'title' => 'Name',
                ])
            ->add('year', Column::class, [
                'title' => 'Year', 'width' => '50px'
                ])
            ->add('price', Column::class, [
                'title' => 'Price', 'width' => '50px'
                ])
            ->add('illustrated', Column::class, [
                'title' => 'Illustrated', 'width' => '70px'
                ])
            ->add('print', Column::class, [
                'title' => 'Print',
                ])
            ->add('part', Column::class, [
                'title' => 'Part', 'width' => '50px'
                ])
            ->add('parts', Column::class, [
                'title' => 'Parts', 'width' => '50px'
                ])
            ->add('author.name', Column::class, [
                'title' => 'Author',
                ])
            ->add('publish.name', Column::class, [
                'title' => 'Publisher',
                ])
            ->add('rack.name', Column::class, [
                'title' => 'Rack',
                'data' => 'rack[, ].name',
                'width' => '50px'
                ])
            ->add(null, ActionColumn::class, [
                'title' => $this->translator->trans('sg.datatables.actions.title'),
                'actions' => [
                    [
                        'route' => 'book_edit',
                        'route_parameters' => [
                            'id' => 'id'
                        ],
                        'label' => $this->translator->trans('sg.datatables.actions.edit'),
                        'icon' => 'glyphicon glyphicon-edit',
                        'attributes' => [
                            'rel' => 'tooltip',
                            'title' => $this->translator->trans('sg.datatables.actions.edit'),
                            'class' => 'btn btn-primary btn-xs',
                            'role' => 'button'
                        ]
                    ]
                ]
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getEntity()
    {
        return 'AppBundle\Entity\Book';
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'book_datatable';
    }
}
