<?php

namespace AppBundle\Twig;

use AppBundle\Entity\File;
use Twig\Extension\AbstractExtension;

class ExtensionExtension extends AbstractExtension {

    /**
     * @return string
     */
    public function getName() {
        return 'app_show_extension';
    }

    /**
     * @return array|\Twig_SimpleFunction[]
     */
    public function getFunctions() {
        return array(
            new \Twig_SimpleFunction('extension', [$this, 'extensionFunction'],
                                     [
                                         'is_safe'           => array('html'),
                                         'needs_environment' => true,]),
        );
    }

    /**
     * @param \Twig_Environment $environment
     * @param File              $file
     *
     * @return string
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function extensionFunction(\Twig_Environment $environment, File $file) {
        $fileAray = [
            'is_image'       => $file->isImage(),
            'file_extension' => $file->getExtention(),
            'file_name'      => $file->getName(),
            'file_id'        => $file->getId(),
            'original_name'  => $file->getOriginalName()];

        return $environment->render(':default/TwigExtension:container_for_files.html.twig', $fileAray);
    }
}