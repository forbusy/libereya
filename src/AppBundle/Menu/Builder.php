<?php
namespace AppBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Translation\Translator;

class Builder implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    public function mainMenu(FactoryInterface $factory, array $options)
    {
        /**@var Translator $trans*/
        $trans = new Translator('en-EN');

        $menu = $factory->createItem('root');
        $menu->setChildrenAttributes(array('class' => 'nav navbar-nav'));

        $menu->addChild($trans->trans('menu.rack'), array('route' => 'rack_index'));
        $menu->addChild($trans->trans('menu.books'), array('route' => 'book_index'));
        $menu->addChild($trans->trans('menu.search'), array('route' => 'search'));

        return $menu;
    }
}