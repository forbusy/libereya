module.exports = function (grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		npmcopy: {
			options: {
				destPrefix: 'web/assets'
			},
			scripts: {
				files: {
					'js/jquery.js': 'jquery/dist/jquery.js',
					'js/jquery-ui.js': 'jquery-ui-dist/jquery-ui.js',
					'js/bootstrap.js': 'bootstrap/dist/js/bootstrap.js',
					'js/moment.js': 'moment/min/moment-with-locales.js',
					'js/datetimepicker.js': 'eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
					'js/bootstrap-ui.js': 'bootstrap-ui/dist/js/bootstrap-ui.js',
					'js/autocompleter.js': '../vendor/pugx/autocompleter-bundle/Resources/public/js/autocompleter-jqueryui.js',
					'js/app.js': '../src/AppBundle/Resources/public/js/autocomplete_parameters.js',
					'js/bootstrap-editable.min.js': '../src/AppBundle/Resources/public/js/bootstrap-editable.min.js',
					'js/datatables.min.js': '../src/AppBundle/Resources/public/js/datatables.min.js',
					'js/featherlight.min.js': '../src/AppBundle/Resources/public/js/featherlight.min.js',
				}
			},
			stylesheets: {
				files: {
					'css/bootstrap.css': 'bootstrap/dist/css/bootstrap.css',
					'css/bootstrap-ui.css': 'bootstrap-ui/dist/css/bootstrap-ui.css',
					'css/login.css': '../src/AppBundle/Resources/public/css/login.css',
					'css/general.css': '../src/AppBundle/Resources/public/css/general.css',
					'css/jquery-ui.css': 'jquery-ui-dist/jquery-ui.css',
					'css/jquery-ui.structure.css': 'jquery-ui-dist/jquery-ui.structure.css',
					'css/jquery-ui.theme.css': 'jquery-ui-dist/jquery-ui.theme.css',
					'css/datatables.min.css': '../src/AppBundle/Resources/public/css/datatables.min.css',
					'css/featherlight.min.css': '../src/AppBundle/Resources/public/css/featherlight.min.css'
				}
			},
			images: {
				files: {
                    'images/dummies.jpg': '../src/AppBundle/Resources/public/img/dummies.jpg'
				}
			},
			fonts:{
				files:{
                    'fonts/fontawesome-webfont.woff2': 'node_modules/font-awesome/fonts/fontawesome-webfont.woff2'
				}
			}
		},
		concat: {
			options: {
				separator: ';'
			},
			css: {
				src:
					[
						'web/assets/css/bootstrap.css',
						'web/assets/css/general.css'
					],
				dest: 'web/assets/css/main.css'
			}
		},
		cssmin : {
			bootstrap:{
				src: 'web/assets/css/main.css',
				dest: 'web/assets/css/main.min.css'
			},
			bootstrapui:{
			    src: 'web/assets/css/bootstrap-ui.css',
                dest: 'web/assets/css/bootstrap-ui.min.css'
            },
			login: {
				src: 'web/assets/css/login.css',
				dest: 'web/assets/css/login.min.css'
			},
			jquery:{
				src: 'web/assets/css/jquery-ui.css',
				dest: 'web/assets/css/jquery-ui.min.css'
			}
		},
		uglify : {
			js: {
				files: {
					'web/assets/js/jquery.min.js': ['web/assets/js/jquery.js'],
					'web/assets/js/bootstrap.min.js': ['web/assets/js/bootstrap.js'],
					'web/assets/js/autocompleter.min.js': ['web/assets/js/autocompleter.js'],
					'web/assets/js/bootstrap-ui.min.js':[
							'web/assets/js/moment.js',
						    'web/assets/js/datetimepicker.js',
						    'web/assets/js/bootstrap-ui.js'
					]
				}
			}
		}
	});

	grunt.loadNpmTasks('grunt-npmcopy');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-uglify');

	grunt.registerTask('default', ['npmcopy', 'concat', 'cssmin', 'uglify']);
};